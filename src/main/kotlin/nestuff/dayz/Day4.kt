package nestuff.dayz
import java.io.FileReader
import java.lang.Exception


/**
 * byr (Birth Year)
 * iyr (Issue Year)
 * eyr (Expiration Year)
 * hgt (Height)
 * hcl (Hair Color)
 * ecl (Eye Color)
 * pid (Passport ID)
 * cid (Country ID)
 */
data class PasoPorto(
    var byr: String? = null,
    var iyr: String? = null,
    var eyr: String? = null,
    var hgt: String? = null,
    var hcl: String? = null,
    var ecl: String? = null,
    var pid: String? = null,
    var cid: String? = null
) {
    fun setValue(maps: Pair<String, String>) {
        val (x, y) = maps
        when(Attribs.valueOf(x)) {
            Attribs.byr -> this.byr = y
            Attribs.iyr -> this.iyr = y
            Attribs.eyr -> this.eyr = y
            Attribs.hgt -> this.hgt = y
            Attribs.hcl -> this.hcl = y
            Attribs.ecl -> this.ecl = y
            Attribs.pid -> this.pid = y
            Attribs.cid -> this.cid = y
        }
    }

    fun reset() {
        this.byr = null
        this.iyr = null
        this.eyr = null
        this.hgt = null
        this.hcl = null
        this.ecl = null
        this.pid = null
        this.cid = null
    }

    fun isValid(): Boolean =
               byr != null
            && iyr != null
            && eyr != null
            && hgt != null
            && hcl != null
            && ecl != null
            && pid != null
                       && byearIsValid()
                       && iyearIsValid()
                       && eyearIsValid()
                       && eyeIsValid()
                       && heightIsValid()
                       && hairIsValid()
                       && passportIsValid()
    fun isInvalid(): Boolean = byr == null ||
    iyr == null ||
    eyr == null ||
    hgt == null ||
    hcl == null ||
    ecl == null ||
    pid == null

    fun byearIsValid(): Boolean = byr?.matchesYears(1920, 2002) ?: false
    fun iyearIsValid(): Boolean = iyr?.matchesYears(2010,2020) ?: false
    fun eyearIsValid(): Boolean = eyr?.matchesYears(2020, 2030) ?: false
    fun heightIsValid(): Boolean = (hgt?.matches("[0-9]+in".toRegex()) == true
            || hgt?.matches("[0-9]+cm".toRegex()) == true)
            && (checkCM() || checkIN())
    private fun checkCM(): Boolean =
        if (hgt?.matches("[0-9]+cm".toRegex()) == true) {
            val height = hgt
            height?.replace("cm", "").let { it?.toInt()!! >= 150 && it.toInt() <= 193 }
        } else false

    private fun checkIN(): Boolean =
        if (hgt?.matches("[0-9]+in".toRegex()) == true) {
            val height = hgt
            height?.replace("in", "").let { it?.toInt()!! >= 59 && it.toInt() <= 76 }
        } else false

    fun hairIsValid(): Boolean = hcl?.matches("(^#([A-Fa-f0-9]){6}\$)".toRegex()) ?: false
    fun eyeIsValid(): Boolean = listOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth").contains(ecl)

    fun passportIsValid(): Boolean = pid?.matches("[0-9]{9}".toRegex()) ?: false


}

enum class Attribs {
    byr,
    iyr,
    eyr,
    hgt,
    hcl,
    ecl,
    pid,
    cid
}

fun readFileAsLinesUsingGetResourceAsStream(filename: String): List<String> = FileReader(filename).readLines()

fun String.matchesYears(minYear: Int, maxYear: Int): Boolean = this.let {
    try {
        it.toInt() in minYear..maxYear
    } catch (ex: Exception) {
        ex.printStackTrace()
        false
    }
}